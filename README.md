# README

## Description
This script scraps the NCBI entrez database using python, urllib and beautifulsoup4 in order to retrieve pubmed identifiers of scientific articles that mention metals and relate to one or several protein in the genome of a given organism.

## Usage
`python3 entrez_query.py -s '<species keywords (e.g. Escherichia coli)>' -e <prof.doctor@institution.edu>`
