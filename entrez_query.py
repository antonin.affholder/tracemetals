#!/usr/bin/env python3

from Bio import Entrez
from Bio import SeqIO

import pandas as pd
import numpy as np
import json

import os
import re
import sys
import time, threading
import concurrent.futures

from urllib import request
from bs4 import BeautifulSoup

from urllib.error import HTTPError

import argparse

watchlist = ['Nickel','nickel','Ni',
             'Magnesium','magnesium','Mg',
             'Iron','iron','Fe',
             'Molybdenum ','molybdenum','Mo',
             'Manganese','manganese','Mn',
             'Copper','copper','Cu',
             'Cobalt','cobalt','Co',
             'Lithium','lithium','Li',
             'Zinc','zinc','Zn',
             'Aluminium','aluminium','Al',
             'Lead','lead','Pb',
             'Gold','gold','Au',
             'Tungsten','tungsten','W',
             'Chromium','Chromium','Cr',
             'Selenium','selenium','Se',
             'Arsenic','arsenic','As']
#'Boron','boron','B', poses problems because of vitamin B

keys = []
for a in watchlist[2::3]:
    keys+=[a]*3
strad = {watchlist[i]:keys[i] for i in range(len(keys))} # To have consistent notation as output

def get_id_from_kwords(name):
    with Entrez.esearch(term = name, db = "genome") as handle:
            tdata  = Entrez.read(handle)
    IDMj = tdata['IdList'][0]
    with Entrez.esummary(id=IDMj,db='genome') as handle:
        tdata = Entrez.read(handle)
    name=tdata[0]['Organism_Name']
    return(IDMj,name)
#print('ID for {} : {}'.format(name,IDMj))

def find_reference_genomes(ID):
    with request.urlopen('https://www.ncbi.nlm.nih.gov/genome/{}'.format(ID)) as page:
        lines = page.read()
        soup = BeautifulSoup(lines,'xml')
        # Now it gets unreadable and frankly, boring. It's just to get the info from the page
        alla = soup.find_all('div','rprt')
    rprsect = alla[0].find_all('div','rprt-section',id='mmtest_repr2')
    if not len(rprsect):
        data = {'NCBI_ID':[],'Name':[],'Entrez_ID':[]}
    else:
        gdescr = rprsect[0]
        gnames = gdescr.find_all('b')
        names = []
        for g in gnames:
            f = g.find_all('i')
            if len(f):
                names.append(str(f[0]).strip('<i>').strip('</'))
        genIDs = [a.contents[0] for a in gdescr.find_all('a',target='_blank')[::2]]
        with Entrez.esummary(id=','.join(genIDs),db='nuccore') as handle:
            tdata = Entrez.read(handle)
        titles = [t['Title'] for t in tdata]
        nuccids = [t['Id'] for t in tdata]
        data = {'NCBI_ID':genIDs,'Name':titles,'Entrez_ID':nuccids}
    return(pd.DataFrame(data=data))

def get_proteins(ID,dbfrom='nuccore'):
    dbto = 'protein'
    linkname = dbfrom+'_'+dbto
    with Entrez.elink(linkname = linkname,id=ID, db = "protein",dbfrom=dbfrom) as handle:
            linkID  = Entrez.read(handle)
    if not len(linkID[0]['LinkSetDb']):
        Ids = np.array([])
    else:
        Ids = [t['Id'] for t in linkID[0]['LinkSetDb'][0]['Link']]
    return(Ids)

def get_protein_info(Ids):
    with Entrez.efetch(id = Ids, db = "protein",rettype = "fasta",retmode='text') as handle:
        # Download the sequence records
        rgen = SeqIO.parse(handle,format='fasta')
        data = {'ID':Ids,'description':[],'length':[],'Gcount':[],'species':[]}
        for r in rgen:
            seq = r.seq
            #data['ID'].append(r.id) # get the ID this is the sequence identifier
            descr = r.description   # get the description

            data['Gcount'].append(seq.count('G')) # count Gly
            data['length'].append(len(seq))       # total length
            species = re.findall('\[.+\]',descr)[0].strip('[]') # parse description to get species name
            ls = len(species.split())
            data['description'].append(' '.join(descr.split()[1:-ls]))    # get the protein description
            data['species'].append(species)
        prots_df = pd.DataFrame(data=data).set_index('ID')
        return(prots_df)
    
def get_protein_info2(Ids):
    """
    Here, we do not care about GC content or whatever, we just get description fields
    """
    data = {'ID':Ids,'description':[],'reference':[],'species':[]}
    with Entrez.efetch(id=Ids,db='protein',rettype="json",retmode='xml') as handle:
        records = Entrez.read(handle)
        for r in records:
            descr  = r['GBSeq_definition']
            species = re.findall('\[.+\]',descr)[0].strip('[]') # parse description to get species name
            ls = len(species.split())
            sdescr = ' '.join(descr.split()[:-ls])
            if 'GBSeq_references' in r.keys() and len(r['GBSeq_references']):
                rtitle = r['GBSeq_references'][0]['GBReference_title'] # Taking only the first one
            else:
                rtitle = ''
            data['description'].append(sdescr)
            data['species'].append(species)
            data['reference'].append(rtitle)
    prots_df = pd.DataFrame(data=data).set_index('ID')
    return(prots_df)


def get_pubmed_ids(Ids):
    with Entrez.elink(id = Ids, dbfrom = "protein",db='pubmed',linkname='protein_pubmed') as handle:
    #rgen = Entrez.read(handle)
    #rgen
        rgen = Entrez.read(handle)
        pubmed_keys = {}
        for r in rgen:
            if len(r['LinkSetDb']):
                pubmed_keys[r['IdList'][0]] = [a['Id'] for a in r['LinkSetDb'][0]['Link']]
            else:
                pubmed_keys[r['IdList'][0]] = []
    return(pubmed_keys)


def get_article_data(pubmed_ids):
    # Titles and abstract are useful to debug, but ultimately we only need the substances
    # Since the request still gets them, it is not entirely useless but meh
    with Entrez.efetch(id=pubmed_ids,db="pubmed",rettype='text') as handle:
        ldata      = Entrez.read(handle)
        #titles     = [a['MedlineCitation']['Article']['ArticleTitle'] for a in ldata['PubmedArticle']]
        substances = []
        #abstracts = []
        
        for b in ldata['PubmedArticle']:
            if 'ChemicalList' in b['MedlineCitation'].keys():
                substances.append([str(a['NameOfSubstance']) for a in b['MedlineCitation']['ChemicalList']])
            else:
                substances.append([])
                
            #if 'Abstract' in b['MedlineCitation'].keys():
            #    abstracts.append(b['MedlineCitation']['Article']['Abstract']['AbstractText'][0])
            #else:
            #    abstracts.append('')
        article_data = {k:{'substances':substances[i]} for i,k in enumerate(pubmed_ids)}
    return(article_data)

def get_flags(article_data,watchlist):
    """
    Gets the substances that match with the watchlist and returns the associated pubmed
    accession number in a dict {'substance':source_pubmed_id}
    """
    flags = {}
    for k in article_data.keys():
        # this is to split susbstances like "molybdneum cofactor", and "Fe-binding protein"
        spsub = []
        for a in article_data[k]['substances']:
            s1 = a.split()
            for s in s1:
                x = s.split('-')
                for c in x:
                    spsub.append(c)
        matches = [strad[a] for a in list(set(spsub).intersection(set(watchlist)))]
        if len(matches):
            for m in matches:
                if m not in flags.keys():
                    flags[m] = [k]
                else:
                    flags[m].append(k)
        else:
            pass
    return(flags)

def get_matchflags(pubmed_keys):
    protein_substances = {}
    errors = []
    for i,k in enumerate(pubmed_keys):
        if len(pubmed_keys[k]):
            try:
                article_data = get_article_data(pubmed_keys[k])
                flags = get_flags(article_data,watchlist)
                protein_substances[k] = flags
            except HTTPError as e:
                protein_substances[k] = {}# e.__cause__
                errors.append(k)

        else:
            protein_substances[k] = {}
            
    return(protein_substances,errors)

def parse_matchflags(protein_substances,errors):
    parsed_data = {}
    for i, (k,v) in enumerate(protein_substances.items()):
        subs = list(v.keys())
        sources = [v[u] for u in subs]
        if len(subs):
            parsed_data[k] = {'substances':'; '.join(['{0} ({1})'.format(subs[j],','.join(sources[j])) for j in range(len(subs))])}
        elif k in errors:
            parsed_data[k] = {'substances':'HTTPerror'}
        else:
            parsed_data[k] = {'substances': np.nan}
    return(parsed_data)

def animated_loading():
    #animation = ["⢿", "⣻", "⣽", "⣾", "⣷", "⣯", "⣟", "⡿"]
    for i in range(5):
        sys.stdout.write('\r working{: <{width}}'.format(''.join(['.']*i),width=5))
        time.sleep(.1)
        sys.stdout.flush()
        
def run_animated(fun,*args):
    with concurrent.futures.ThreadPoolExecutor() as executor:
        future = executor.submit(fun, *args)
        while future.running():
            animated_loading()
        print('')
        print('finished')
        return_value = future.result()
    return(return_value)

    
    
                        

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser(description='Get proteins that interact with or require metals/trace metals in a given organism.')
    parser.add_argument('--search', '-s', type=str, help='species name to query')
    parser.add_argument('--email','-e', type=str, help='email for entrez query (institutional email)')
    
    args = parser.parse_args()
    email = args.email
    Entrez.email = email
    name = args.search
    ID,name = get_id_from_kwords(name)
    print('ID for {} (closest match for keywords) : {}'.format(name,ID))
    
    autofilename = name.split()[0][0]+name.split()[1]+'_proteindata.json'
    autopath = os.path.join('data/',autofilename)
    fetched = os.path.exists(autopath)
    repair = False
    if fetched:
        prot_merged = pd.read_json(autopath,orient='table')
        pmerge_sum = prot_merged.dropna()
        print(pmerge_sum)
        print('Looks like this query has already be done, and is stored at {}'.format(autopath))
        resume = input('Do you want to overwrite this file? press <o>. To attempt error repair type <r>. Any other key to abort\n')
        if resume == 'o':
            pass
        elif resume == 'r':
            repair = True
            Ids = list(prot_merged.dropna().query('substances=="HTTPerror"').index)
        else:
            sys.exit()
    else:
        pass
    if not repair:
        print('Looking for reference genomes')
        refG_df = run_animated(find_reference_genomes,ID)
        if len(refG_df):
            print('Found the following:')
            print(refG_df)
            ig = int(input('Input index of selected reference genome'))
            ID_refG = refG_df.loc[ig]['Entrez_ID']
            dbfrom = 'nuccore'
        else:
            print('No particular reference genome found, looking at links directly from genome DB')
            ID_refG = ID
            dbfrom='genome'
        print('Querying all protein IDs in the genome')
        Ids = run_animated(get_proteins,ID_refG,dbfrom)
    else:# If repair mode
        pass
    print('Got {} protein IDs'.format(len(Ids)))
    if not len(Ids):
        print('No protein found (probably not yet linked in the database) EXITING')
        sys.exit()
    if not repair:
        print('Querying protein info')
        prot_df = run_animated(get_protein_info2,Ids)
    print('Got protein info')
    print('Querying associated publications')
    pubmed_keys = run_animated(get_pubmed_ids,Ids)
    print('Got associated pubications')
    print('Testing for watchlist:')
    print(watchlist[::3])
    protein_substances,errors = run_animated(get_matchflags,pubmed_keys)
    print('Got matching substances ({} errors):'.format(len(errors)))
    parsed_data = parse_matchflags(protein_substances,errors)
    to_merge = pd.DataFrame.from_dict(data=parsed_data,orient='index')
    if repair:
        prot_merged['substances'].loc[to_merge.index] = to_merge['substances']
    else:
        prot_merged = pd.merge(prot_df,to_merge,left_index=True,right_index=True)
        prot_merged.index.name = 'protein id'
    print(prot_merged.dropna())
    prot_merged.to_json(autopath,orient='table')
    print('saved data to {}'.format(autopath))
    print('Finished')
    